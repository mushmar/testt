import Vue from 'vue'
import Vuex from 'vuex'
const axios = require('axios');

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    user: null,
    form: null
  },
  mutations: {},
  actions: {
    SIGN_IN(state, data) {
      return new Promise((resolve, reject) => {
          axios.post('http://pink-code.ru:20085/auth', {
              username: data.email,
              password: data.password,
          })
            .then( response => {
              localStorage.setItem('user', JSON.stringify(response.data))
              resolve(response)
            })
            .catch( (error) => {
              reject(error)
            })
      })

    },
    SEND_APPLICATION(state, data) {
      return new Promise((resolve, reject) => {
          axios.post('http://pink-code.ru:20085/request',  {
              phone: data.phone,
              name: data.fullName,
              dob: data.date,
              eventId: data.tab.id,
              opt1: data.parking ? 1 : 0,
              opt2: data.material ? 1 : 0,
              opt3: data.accompanying ? 1 : 0
          }, {
              headers: {
                  'Authorization': `token ${JSON.parse(localStorage.getItem('user')).token}`
              }
          })
              .then( response => {
                  state.form = response.data
                  localStorage.setItem('form', JSON.stringify(response.data))
                  resolve(response)
              })
              .catch( (error) => {
                  reject(error)
              })
      })
    },
    GET_ACTIVITY() {
      return new Promise((resolve, reject) => {
          axios.get('http://pink-code.ru:20085/list', {
              headers: {
                  'Authorization': `token ${JSON.parse(localStorage.getItem('user')).token}`
              }
          })
              .then( response => {
                  resolve(response)
              })
              .catch( (error) => {
                  reject(error)
              })
      })

    },
  },
  getters: {
    form: (state) => {
      return state.form
    },
  }
})
